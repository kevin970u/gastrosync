$.validator.addMethod(
  "lettersonly",
  function (value, element) {
    return this.optional(element) || /^[a-zA-Z\s]*$/.test(value);
  },
  "Solo se permiten letras en este campo"
);
$("#form-add-tipo").validate({
  rules: {
    nombre_tipla: {
      required: true,
      maxlength: 50,
      lettersonly: true,
    },
    descripcion_tipla: {
      required: true,
      maxlength: 250,
    },
    color_tipla: {
      required: true,
      maxlength: 50,
    },
    imagen_tipla: {},
  },
  messages: {
    nombre_tipla: {
      required: "Por favor ingrese el nombre del tipo de platillo",
      maxlength:
        "El nombre del tipo de platillo debe tener menos de 50 caracteres",
      lettersonly: "El nombre del tipo de platillo debe contener solo letras",
    },
    descripcion_tipla: {
      required: "Por favor ingrese la descripcion del tipo de platillo",
      maxlength:
        "La descripcion del tipo de platillo debe tener menos de 250 caracteres",
    },
    color_tipla: {
      required: "Por favor ingrese el color del tipo de platillo",
      maxlength:
        "El color del tipo de platillo debe tener menos de 50 caracteres",
    },
    imagen_tipla: {
      required: "Por favor ingrese la imagen del tipo de platillo",
    },
  },
});

$("#form-add-empleado").validate({
  rules: {
    nombres_emp: {
      required: true,
      maxlength: 50,
      lettersonly: true,
    },
    apeliidos_emp: {
      required: true,
      maxlength: 250,
      lettersonly: true,
    },
    cargo_emp: {
      required: true,
      maxlength: 50,
      lettersonly: true,
    },
    telefono_emp: {
      required: true,
      maxlength: 10,
      minlength: 10,
      number: true,
    },
    correo_electronico_emp: {
      required: true,
      maxlength: 50,
      email: true,
    },
    fecha_ingreso_emp: {
      required: true,
      maxlength: 50,
    },
    salario_emp: {
      required: true,
      digits: true,
      max: 10000,
      number: true,
      min: 0,
    },
    foto_emp: {},
  },
  messages: {
    nombres_emp: {
      required: "Por favor ingrese los nombres del empleado",
      maxlength: "Los nombres del empleado debe tener menos de 50 caracteres",
      lettersonly: "Los nombres del empleado debe contener solo letras",
    },
    apeliidos_emp: {
      required: "Por favor ingrese los apellidos del empleado",
      maxlength: "Los apellidos del empleado debe tener menos de 50 caracteres",
      lettersonly: "Los apellidos del empleado debe contener solo letras",
    },
    cargo_emp: {
      required: "Por favor ingrese el cargo del empleado",
      maxlength: "El cargo debe tener menos de 50 caracteres",
      lettersonly: "El cargo del empleado debe contener solo letras",
    },
    telefono_emp: {
      required: "Por favor ingrese el telefono del empleado",
      maxlength: "El telefono del empleado debe tener maximo 10 numeros",
      minlength: "El telefono del empleado debe tener minimo 10 numeros",
      number: "Solo se permite numeros en este campo",
    },
    correo_electronico_emp: {
      required: "Por favor ingrese el correo electronico del empleado",
      maxlength:
        "El correo electronico del empleado debe tener menos de 50 caracteres",
      email: "Debe ingresar un correo valido",
    },
    fecha_ingreso_emp: {
      required: "Por favor ingrese la fecha de ingreso del empleado",
      maxlength:
        "La fecha de ingreso del empleado debe tener menos de 50 caracteres",
    },
    salario_emp: {
      required: "Por favor ingrese el salario del empleado",
      digits: "Solo se permite numeros en este campo",
      number: "Solo se permite numeros en este campo",
      max: "El salario no puede ser mayor a 10000",
      min: "El salario no puede ser menor a 0",
    },
    foto_emp: {
      required: "Por favor seleccione la foto del empleado",
    },
  },
});

$("#form-add-mesa").validate({
  rules: {
    capacidad_mes: {
      required: true,
      maxlength: 50,
      number: true,
    },
    estado_mes: {
      required: true,
      maxlength: 50,
      lettersonly: true,
    },
    numero_mes: {
      required: true,
      maxlength: 50,
      number: true,
    },
    descripcion_mes: {
      required: true,
      maxlength: 250,
      lettersonly: true,
    },
  },
  messages: {
    capacidad_mes: {
      required: "Por favor ingrese la capacidad de la mesa",
      maxlength: "La capacidad de la mesa debe tener menos de 50 caracteres",
      number: "Solo se permite numeros en este campo",
    },
    estado_mes: {
      required: "Por favor ingrese el estado de la mesa",
      maxlength: "El estado de la mesa debe tener menos de 50 caracteres",
      lettersonly: "El estado de la mesa debe contener solo letras",
    },
    numero_mes: {
      required: "Por favor ingrese el numero de mesa",
      maxlength: "El numero de mesa debe tener menos de 50 caracteres",
      number: "Solo se permite numeros en este campo",
    },
    descripcion_mes: {
      required: "Por favor ingrese la descripcion de la mesa",
      maxlength: "La descripcion de la mesa debe tener menos de 50 caracteres",
      lettersonly: "La descripcion de la mesa debe contener solo letras",
    },
  },
});

$("#form-add-cliente").validate({
  rules: {
    nombres_cli: {
      required: true,
      maxlength: 50,
      lettersonly: true,
    },
    apeliidos_cli: {
      required: true,
      maxlength: 250,
      lettersonly: true,
    },
    telefono_cli: {
      required: true,
      maxlength: 10,
      minlength: 10,
      number: true,
    },
    direccion_cli: {
      required: true,
      maxlength: 50,
      lettersonly: true,
    },
    correo_electronico_cli: {
      required: true,
      maxlength: 50,
      email: true,
    },
  },
  messages: {
    nombres_cli: {
      required: "Por favor ingrese los nombres del cliente",
      maxlength: "Los nombres del cliente debe tener menos de 50 caracteres",
      lettersonly: "Los nombres del cliente debe contener solo letras",
    },
    apeliidos_cli: {
      required: "Por favor ingrese los apellidos del cliente",
      maxlength: "Los apellidos del cliente debe tener menos de 50 caracteres",
      lettersonly: "Los apellidos del cliente debe contener solo letras",
    },
    telefono_cli: {
      required: "Por favor ingrese el telefono del cliente",
      maxlength: "El telefono del cliente debe tener maximo de 10 numeros",
      minlength: "El telefono del cliente debe tener minimo de 10 numeros",
      number: "Solo se permite numeros en este campo",
    },
    direccion_cli: {
      required: "Por favor ingrese la direccion del cliente",
      maxlength: "La direccion del cliente debe tener menos de 50 caracteres",
      lettersonly: "La direccion del cliente debe contener solo letras",
    },
    correo_electronico_cli: {
      required: "Por favor ingrese el correo electronico del cliente",
      maxlength:
        "El correo electronico del cliente debe tener menos de 50 caracteres",
      email: "Debe ingresar un correo valido",
    },
  },
});

$("#form-send-email").validate({
  rules: {
    destinatario: {
      required: true,
      email: true,
    },
    asunto: {
      required: true,
      maxlength: 250,
    },
    cuerpo: {
      required: true,
    },
  },
  messages: {
    destinatario: {
      required: "Por favor ingrese el correo del destinatario",
      email: "Debe ingresar un correo valido",
    },
    asunto: {
      required: "Por favor ingrese el asunto del correo",
      maxlength: "El asunto del correo debe tener menos de 250 caracteres",
    },
    cuerpo: {
      required: "Por favor ingrese el cuerpo del correo",
    },
  },
});


$("#form-add-pedido").validate({
  rules: {
    id_cli: {
      required: true,
    },
    id_pla: {
      required: true,
    },
    fecha_ped: {
      required: true,
      date: true,
    },
    estado_ped: {
      required: true,
      maxlength: 50,
      lettersonly: true,
    },
    comentarios_ped: {
      required: true,
      maxlength: 150,
      lettersonly: true,
    },
  },
  messages: {
    id_cli: {
      required: "Por favor seleccione un cliente",
    },
    id_pla: {
      required: "Por favor seleccione un platillo",
    },
    fecha_ped: {
      required: "Por favor ingrese la fecha del pedido",
    },
    estado_ped: {
      required: "Por favor ingrese el estado del pedido, ejem: Preparando",
      maxlength: "El estado del pedido debe tener menos de 50 caracteres",
      lettersonly: "Debe contener solo letras",
    },
    comentarios_ped: {
      required: "Por favor ingrese un comentario para el pedido",
      maxlength: "El comentario del pedido debe tener maximo de 150 caracteres",
      lettersonly: "Debe contener solo letras",
    },
  },
});


$("#form-add-reserva").validate({
  rules: {
    id_cli: {
      required: true,
    },
    id_mes: {
      required: true,
    },
    fecha_res: {
      required: true,
      date: true,
    },
    numero_personas_res: {
      required: true,
      max: 12,
      number: true,
    },
    estado_res: {
      required: true,
      maxlength: 50,
      lettersonly: true,
    },
  },
  messages: {
    id_cli: {
      required: "Por favor seleccione un cliente",
    },
    id_mes: {
      required: "Por favor seleccione una mesa",
    },
    fecha_res: {
      required: "Por favor ingrese la fecha de la reserva",
    },
    numero_personas_res: {
      required: "Por favor ingrese el estado del pedido, ejem: Preparando",
      max: "El numero de personas para la mesa es de maximo 12 personas",
      number: "Debe contener solo numeros",
    },
    estado_res: {
      required: "Por favor ingrese el estado de la reserva, ejem: Reservado",
      maxlength: "El estado de la reserva debe tener maximo de 50 caracteres",
      lettersonly: "Debe contener solo letras",
    },
  },
});


$("#form-add-platillo").validate({
  rules: {
    id_tipla: {
      required: true,
    },
    nombre_pla: {
      required: true,
      maxlength: 50,
      lettersonly: true,
    },
    descripcion_pla: {
      required: true,
      maxlength: 100,
      lettersonly: true,
    },
    precio_pla: {
      required: true,
      max: 20,
      min: 5,
      number: true,
    },
    imagen_pla: {
      required: true,
    },
  },
  messages: {
    id_tipla: {
      required: "Por favor seleccione un tipo de platillo",
    },
    nombre_pla: {
      required: "Por favor ingrese el nombre del platillo",
      maxlength: "El nombre del platillo debe tener maximo 20 caracteres",
      lettersonly: "Debe contener solo letras",
    },
    descripcion_pla: {
      required: "Por favor ingrese una descripcion del platillo",
      maxlength: "La descripcion del platillo debe tener maximo 100 caracteres",
      lettersonly: "Debe contener solo letras",
    },
    precio_pla: {
      required: "Por favor ingrese el precio del platillo",
      max: "El precio maximo de un platillo es de 20 dolares",
      min: "El precio minimo de un platillo es de 5 dolares",
      number: "Debe contener solo numeros",
    },
    imagen_pla: {
      required: "Por favor seleccione una imagen para el platillo",
    },
  },
});


$("#form-add-factura").validate({
  rules: {
    id_ped: {
      required: true,
    },
    fecha_fac: {
      required: true,
      date: true,
    },
    metodo_pago_fac: {
      required: true,
      maxlength: 50,
      lettersonly: true,
    },
  },
  messages: {
    id_ped: {
      required: "Por favor seleccione un pedido",
    },
    fecha_fac: {
      required: "Por favor ingrese la fecha para la factura",
    },
    metodo_pago_fac: {
      required: "Por favor ingrese el metodo de pago, ejem: PayPal",
      maxlength: "El metodo de pago debe tener maximo 50 caracteres",
      lettersonly: "Debe contener solo letras",
    },
  },
});