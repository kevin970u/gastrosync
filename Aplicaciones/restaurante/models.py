from django.db import models

# Create your models here.
class TiposPlatillo(models.Model):
    id_tipla=models.AutoField(primary_key=True)
    nombre_tipla=models.CharField(max_length=50)
    descripcion_tipla=models.CharField(max_length=250)
    color_tipla=models.CharField(max_length=50)
    imagen_tipla=models.FileField(upload_to='tipos-platillos', null=True,blank=True)

    def __str__(self):
        return self.nombre_tipla

#Modelo Platillo  
class Platillo(models.Model):
    id_pla=models.AutoField(primary_key=True)
    nombre_pla=models.CharField(max_length=50)
    descripcion_pla=models.CharField(max_length=50)
    precio_pla=models.CharField(max_length=50)
    imagen_pla=models.FileField(upload_to='platillos', null=True,blank=True)
    tiposplatillos=models.ForeignKey(TiposPlatillo,null=True,blank=True,on_delete=models.PROTECT)

# Modelo Empleado
class Empleado(models.Model):
    id_emp=models.AutoField(primary_key=True)
    nombres_emp=models.CharField(max_length=50)
    apeliidos_emp=models.CharField(max_length=50)
    cargo_emp=models.CharField(max_length=50)
    telefono_emp=models.CharField(max_length=50)
    correo_electronico_emp=models.CharField(max_length=100)
    fecha_ingreso_emp=models.CharField(max_length=50)
    salario_emp=models.CharField(max_length=50)
    foto_emp=models.FileField(upload_to='empleados', null=True,blank=True)

    def __str__(self):
        return self.nombres_emp

# Modelo Mesa
class Mesa(models.Model):
    id_mes=models.AutoField(primary_key=True)
    capacidad_mes=models.CharField(max_length=50)
    estado_mes=models.CharField(max_length=50)
    numero_mes=models.CharField(max_length=50)
    descripcion_mes=models.CharField(max_length=50)

    def __str__(self):
        return self.numero_mes

# Modelo Cliente
class Cliente(models.Model):
    id_cli=models.AutoField(primary_key=True)
    nombres_cli=models.CharField(max_length=50)
    apeliidos_cli=models.CharField(max_length=50)
    telefono_cli=models.CharField(max_length=50)
    direccion_cli=models.CharField(max_length=50)
    correo_electronico_cli=models.CharField(max_length=100)

    def __str__(self):
        return self.nombres_cli

# Modelo Reserva
class Reserva(models.Model):
    id_res=models.AutoField(primary_key=True)
    fecha_res=models.CharField(max_length=50)
    numero_personas_res=models.CharField(max_length=50)
    estado_res=models.CharField(max_length=50)
    cliente=models.ForeignKey(Cliente,null=True,blank=True,on_delete=models.PROTECT)
    mesa=models.ForeignKey(Mesa,null=True,blank=True,on_delete=models.PROTECT)

    def __str__(self):
        return self.fecha_res

# Modelo Pedido
class Pedido(models.Model):
    id_ped=models.AutoField(primary_key=True)
    fecha_ped=models.CharField(max_length=50)
    estado_ped=models.CharField(max_length=50)
    comentarios_ped=models.CharField(max_length=50)
    cliente=models.ForeignKey(Cliente,null=True,blank=True,on_delete=models.PROTECT)
    platillo=models.ForeignKey(Platillo,null=True,blank=True,on_delete=models.PROTECT)

    def __str__(self):
        return self.fecha_ped

# Modelo Factura
class Factura(models.Model):
    id_fac=models.AutoField(primary_key=True)
    fecha_fac=models.CharField(max_length=50)
    metodo_pago_fac=models.CharField(max_length=50)
    pedido=models.ForeignKey(Pedido,null=True,blank=True,on_delete=models.PROTECT)

    def __str__(self):
        return self.fecha_fac



    