from django.urls import path
from . import views

urlpatterns = [
    path('',views.home),

    #Todo: Tipos Platillo
    path('tiposplatillos/', views.listadoTiposPlatillo, name='tiposplatillos'),
    path('crearTipoPlatillo/', views.crearTipoPlatillo, name='crearTipoPlatillo'),
    path('eliminarTipoPlatillo/<id_tipla>/', views.eliminarTipoPlatillo, name='eliminarTipoPlatillo'),
    path('actualizarTipoPlatillo/<id_tipla>/', views.actualizarTipoPlatilloView, name='actualizarTipoPlatillo'),
    path('procesarActualizacionTipoPlatillo/', views.procesarActualizacionTipoPlatillo, name='procesarActualizacionTipoPlatillo'),
    #Todo: Empleado
    path('empleado/', views.listadoEmpleado, name='empleado'),
    path('crearEmpleado/', views.crearEmpleado, name='crearEmpleado'),
    path('eliminarEmpleado/<id_emp>/', views.eliminarEmpleado, name='eliminarEmpleado'),
    path('actualizarEmpleado/<id_emp>/', views.actualizarEmpleadoView, name='actualizarEmpleado'),
    path('procesarActualizacionEmpleado/', views.procesarActualizacionEmpleado, name='procesarActualizacionEmpleado'),
    #Todo: Mesa
    path('mesa/', views.listadoMesa, name='mesa'),
    path('crearMesa/', views.crearMesa, name='crearMesa'),
    path('eliminarMesa/<id_mes>/', views.eliminarMesa, name='eliminarMesa'),
    path('actualizarMesa/<id_mes>/', views.actualizarMesaView, name='actualizarMesa'),
    path('procesarActualizacionMesa/', views.procesarActualizacionMesa, name='procesarActualizacionMesa'),
    #Todo: Cliente
    path('cliente/', views.listadoCliente, name='cliente'),
    path('crearCliente/', views.crearCliente, name='crearCliente'),
    path('eliminarCliente/<id_cli>/', views.eliminarCliente, name='eliminarCliente'),
    path('actualizarCliente/<id_cli>/', views.actualizarClienteView, name='actualizarCliente'),
    path('procesarActualizacionCliente/', views.procesarActualizacionCliente, name='procesarActualizacionCliente'),
    #Todo: Platillo
    path('platillo/', views.listadoPlatillo, name='platillo'),
    path('crearPlatillo/', views.crearPlatillo, name='crearPlatillo'),
    path('eliminarPlatillo/<id_pla>/', views.eliminarPlatillo, name='eliminarPlatillo'),
    path('actualizarPlatillo/<id_pla>/', views.actualizarPlatilloView, name='actualizarPlatillo'),
    path('procesarActualizacionPlatillo/', views.procesarActualizacionPlatillo, name='procesarActualizacionPlatillo'),
    #Todo: Reserva
    path('reserva/', views.listadoReserva, name='reserva'),
    path('crearReserva/', views.crearReserva, name='crearReserva'),
    path('eliminarReserva/<id_res>/', views.eliminarReserva, name='eliminarReserva'),
    path('actualizarReserva/<id_res>/', views.actualizarReservaView, name='actualizarReserva'),
    path('procesarActualizacionReserva/', views.procesarActualizacionReserva, name='procesarActualizacionReserva'),
    #Todo: Pedido
    path('pedido/', views.listadoPedido, name='pedido'),
    path('crearPedido/', views.crearPedido, name='crearPedido'),
    path('eliminarPedido/<id_ped>/', views.eliminarPedido, name='eliminarPedido'),
    path('actualizarPedido/<id_ped>/', views.actualizarPedidoView, name='actualizarPedido'),
    path('procesarActualizacionPedido/', views.procesarActualizacionPedido, name='procesarActualizacionPedido'),
    #Todo: Factura
    path('factura/', views.listadoFactura, name='factura'),
    path('crearFactura/', views.crearFactura, name='crearFactura'),
    path('eliminarFactura/<id_fac>/', views.eliminarFactura, name='eliminarFactura'),
    path('actualizarFactura/<id_fac>/', views.actualizarFacturaView, name='actualizarFactura'),
    path('procesarActualizacionFactura/', views.procesarActualizacionFactura, name='procesarActualizacionFactura'),
    #Todo: Enviar Mensaje
    path('enviarMensaje/', views.enviarMensajeView, name='enviarMensaje'),
    path('procesarEnvioMensaje/', views.enviar_correo, name='procesarEnvioMensaje'),
]