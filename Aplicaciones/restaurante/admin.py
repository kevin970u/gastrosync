from django.contrib import admin
from .models import TiposPlatillo, Empleado, Mesa, Cliente, Platillo, Reserva, Pedido, Factura

# Register your models here.
admin.site.register(TiposPlatillo)

# CRUD Empleado
admin.site.register(Empleado)

# CRUD Mesa
admin.site.register(Mesa)

# CRUD Cliente
admin.site.register(Cliente)

# CRUD Cliente
admin.site.register(Platillo)

# CRUD Reserva
admin.site.register(Reserva)

# CRUD Reserva
admin.site.register(Pedido)

# CRUD Reserva
admin.site.register(Factura)

