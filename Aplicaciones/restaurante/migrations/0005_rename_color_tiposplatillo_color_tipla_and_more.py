# Generated by Django 4.2.10 on 2024-02-11 17:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restaurante', '0004_cliente'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tiposplatillo',
            old_name='color',
            new_name='color_tipla',
        ),
        migrations.RenameField(
            model_name='tiposplatillo',
            old_name='descripcion',
            new_name='descripcion_tipla',
        ),
        migrations.RenameField(
            model_name='tiposplatillo',
            old_name='id',
            new_name='id_tipla',
        ),
        migrations.RenameField(
            model_name='tiposplatillo',
            old_name='imagen',
            new_name='imagen_tipla',
        ),
        migrations.RenameField(
            model_name='tiposplatillo',
            old_name='nombre',
            new_name='nombre_tipla',
        ),
    ]
