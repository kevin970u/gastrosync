# Generated by Django 4.2.10 on 2024-02-15 03:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('restaurante', '0012_pedido'),
    ]

    operations = [
        migrations.CreateModel(
            name='Factura',
            fields=[
                ('id_fac', models.AutoField(primary_key=True, serialize=False)),
                ('fecha_fac', models.CharField(max_length=50)),
                ('metodo_pago_fac', models.CharField(max_length=50)),
                ('pedido', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='restaurante.pedido')),
            ],
        ),
    ]
