# Generated by Django 4.2.10 on 2024-02-11 17:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('restaurante', '0005_rename_color_tiposplatillo_color_tipla_and_more'),
    ]

    operations = [
        migrations.RenameField(
            model_name='empleado',
            old_name='apeliidos',
            new_name='apeliidos_emp',
        ),
        migrations.RenameField(
            model_name='empleado',
            old_name='cargo',
            new_name='cargo_emp',
        ),
        migrations.RenameField(
            model_name='empleado',
            old_name='correo_electronico',
            new_name='correo_electronico_emp',
        ),
        migrations.RenameField(
            model_name='empleado',
            old_name='fecha_ingreso',
            new_name='fecha_ingreso_emp',
        ),
        migrations.RenameField(
            model_name='empleado',
            old_name='foto',
            new_name='foto_emp',
        ),
        migrations.RenameField(
            model_name='empleado',
            old_name='id',
            new_name='id_emp',
        ),
        migrations.RenameField(
            model_name='empleado',
            old_name='nombres',
            new_name='nombres_emp',
        ),
        migrations.RenameField(
            model_name='empleado',
            old_name='salario',
            new_name='salario_emp',
        ),
        migrations.RenameField(
            model_name='empleado',
            old_name='telefono',
            new_name='telefono_emp',
        ),
    ]
