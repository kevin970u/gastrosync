from django.shortcuts import render, redirect
from .models import TiposPlatillo, Empleado, Mesa, Cliente, Platillo, Reserva, Pedido, Factura
from django.contrib import messages
from django.core.mail import send_mail
from django.conf import settings


# Create your views here.

def home(request):
    return render(request,'home.html')

#Todo: Tipos Platillo
def listadoTiposPlatillo(request):
    tiposPlatilloDbb = TiposPlatillo.objects.all()
    return render(request, 'tiposPlatillo/listadoTiposPlatillo.html', {
        'tiposplatillos': list(enumerate(tiposPlatilloDbb, start=1))
    })

def crearTipoPlatillo(request):
    nombre_tipla = request.POST['nombre_tipla']
    descripcion_tipla = request.POST['descripcion_tipla']
    color_tipla = request.POST['color_tipla']
    imagen_tipla = request.FILES.get('imagen_tipla')

    TiposPlatillo.objects.create(nombre_tipla=nombre_tipla, descripcion_tipla=descripcion_tipla, color_tipla=color_tipla, imagen_tipla=imagen_tipla)
    messages.success(request, 'Tipo de platillos agregado correctamente')
    return redirect('/tiposplatillos')

def eliminarTipoPlatillo(request, id_tipla):
    tipoPlatilloDb = TiposPlatillo.objects.get(id_tipla=id_tipla)
    tipoPlatilloDb.delete()
    messages.success(request, 'Tipo de platillo eliminado correctamente')
    return redirect('/tiposplatillos')

def actualizarTipoPlatilloView(request, id_tipla):
    tipoPlatilloDb = TiposPlatillo.objects.get(id_tipla=id_tipla)

    return render(request, 'tiposPlatillo/editarTiposPlatillo.html', {
        'tiposplatillos': tipoPlatilloDb
    })

def procesarActualizacionTipoPlatillo(request):
    id_tipla = request.POST['id_tipla']
    nombre_tipla = request.POST['nombre_tipla']
    descripcion_tipla = request.POST['descripcion_tipla']
    color_tipla = request.POST['color_tipla']
    imagen_tipla = request.FILES.get('imagen_tipla')

    tipoPlatilloUpdate = TiposPlatillo.objects.get(id_tipla=id_tipla)
    tipoPlatilloUpdate.nombre_tipla = nombre_tipla
    tipoPlatilloUpdate.descripcion_tipla = descripcion_tipla
    tipoPlatilloUpdate.color_tipla = color_tipla
    if imagen_tipla is not None:
        tipoPlatilloUpdate.imagen_tipla = imagen_tipla

    tipoPlatilloUpdate.save()

    messages.success(request, 'Tipo de platillo actualizado correctamente')
    return redirect('/tiposplatillos')



#Todo: Empleado
def listadoEmpleado(request):
    empleadoDbb = Empleado.objects.all()
    return render(request, 'empleado/listadoEmpleado.html', {
        'empleado': list(enumerate(empleadoDbb, start=1))
    })

def crearEmpleado(request):
    nombres_emp = request.POST['nombres_emp']
    apeliidos_emp = request.POST['apeliidos_emp']
    cargo_emp = request.POST['cargo_emp']
    telefono_emp = request.POST['telefono_emp']
    correo_electronico_emp = request.POST['correo_electronico_emp']
    fecha_ingreso_emp = request.POST['fecha_ingreso_emp']
    salario_emp = request.POST['salario_emp']
    foto_emp = request.FILES.get('foto_emp')

    Empleado.objects.create(nombres_emp=nombres_emp, apeliidos_emp=apeliidos_emp, cargo_emp=cargo_emp, telefono_emp=telefono_emp, correo_electronico_emp=correo_electronico_emp, fecha_ingreso_emp=fecha_ingreso_emp, salario_emp=salario_emp, foto_emp=foto_emp)
    messages.success(request, 'Empleado agregado correctamente')
    return redirect('/empleado')

def eliminarEmpleado(request, id_emp):
    empleadoDb = Empleado.objects.get(id_emp=id_emp)
    empleadoDb.delete()
    messages.success(request, 'Empleado eliminado correctamente')
    return redirect('/empleado')

def actualizarEmpleadoView(request, id_emp):
    empleadoDb = Empleado.objects.get(id_emp=id_emp)

    return render(request, 'empleado/editarEmpleado.html', {
        'empleado': empleadoDb
    })

def procesarActualizacionEmpleado(request):
    id_emp = request.POST['id_emp']
    nombres_emp = request.POST['nombres_emp']
    apeliidos_emp = request.POST['apeliidos_emp']
    cargo_emp = request.POST['cargo_emp']
    telefono_emp = request.POST['telefono_emp']
    correo_electronico_emp = request.POST['correo_electronico_emp']
    fecha_ingreso_emp = request.POST['fecha_ingreso_emp']
    salario_emp = request.POST['salario_emp']
    foto_emp = request.FILES.get('foto_emp')

    empleadoUpdate = Empleado.objects.get(id_emp=id_emp)
    empleadoUpdate.nombres_emp = nombres_emp
    empleadoUpdate.apeliidos_emp = apeliidos_emp
    empleadoUpdate.cargo_emp = cargo_emp
    empleadoUpdate.telefono_emp = telefono_emp
    empleadoUpdate.correo_electronico_emp = correo_electronico_emp
    empleadoUpdate.fecha_ingreso_emp = fecha_ingreso_emp
    empleadoUpdate.salario_emp = salario_emp
    if foto_emp is not None:
        empleadoUpdate.foto_emp = foto_emp

    empleadoUpdate.save()

    messages.success(request, 'Empleado actualizado correctamente')
    return redirect('/empleado')



#Todo: Mesa
def listadoMesa(request):
    mesaDbb = Mesa.objects.all()
    return render(request, 'mesa/listadoMesa.html', {
        'mesa': list(enumerate(mesaDbb, start=1))
    })

def crearMesa(request):
    capacidad_mes = request.POST['capacidad_mes']
    estado_mes = request.POST['estado_mes']
    numero_mes = request.POST['numero_mes']
    descripcion_mes = request.POST['descripcion_mes']

    Mesa.objects.create(capacidad=capacidad_mes, estado_mes=estado_mes, numero_mes=numero_mes, descripcion_mes=descripcion_mes)
    messages.success(request, 'Mesa agregada correctamente')
    return redirect('/mesa')

def eliminarMesa(request, id_mes):
    mesaDb = Mesa.objects.get(id_mes=id_mes)
    mesaDb.delete()
    messages.success(request, 'Mesa eliminada correctamente')
    return redirect('/mesa')

def actualizarMesaView(request, id_mes):
    mesaDb = Mesa.objects.get(id_mes=id_mes)

    return render(request, 'mesa/editarMesa.html', {
        'mesa': mesaDb
    })

def procesarActualizacionMesa(request):
    id_mes = request.POST['id_mes']
    capacidad_mes = request.POST['capacidad_mes']
    estado_mes = request.POST['estado_mes']
    numero_mes = request.POST['numero_mes']
    descripcion_mes = request.POST['descripcion_mes']

    mesaUpdate = Mesa.objects.get(id_mes=id_mes)
    mesaUpdate.capacidad_mes = capacidad_mes
    mesaUpdate.estado_mes = estado_mes
    mesaUpdate.numero_mes = numero_mes
    mesaUpdate.descripcion_mes = descripcion_mes
    mesaUpdate.save()

    messages.success(request, 'Mesa actualizada correctamente')
    return redirect('/mesa')


#Todo: Cliente
def listadoCliente(request):
    clienteDbb = Cliente.objects.all()
    return render(request, 'cliente/listadoCliente.html', {
        'cliente': list(enumerate(clienteDbb, start=1))
    })

def crearCliente(request):
    nombres_cli = request.POST['nombres_cli']
    apeliidos_cli = request.POST['apeliidos_cli']
    telefono_cli = request.POST['telefono_cli']
    direccion_cli = request.POST['direccion_cli']
    correo_electronico_cli = request.POST['correo_electronico_cli']

    Cliente.objects.create(nombres_cli=nombres_cli, apeliidos_cli=apeliidos_cli, telefono_cli=telefono_cli, direccion_cli=direccion_cli,  correo_electronico_cli=correo_electronico_cli)
    messages.success(request, 'Cliente agregado correctamente')
    return redirect('/cliente')

def eliminarCliente(request, id_cli):
    clienteDb = Cliente.objects.get(id_cli=id_cli)
    clienteDb.delete()
    messages.success(request, 'Cliente eliminado correctamente')
    return redirect('/cliente')

def actualizarClienteView(request, id_cli):
    clienteDb = Cliente.objects.get(id_cli=id_cli)

    return render(request, 'cliente/editarCliente.html', {
        'cliente': clienteDb
    })

def procesarActualizacionCliente(request):
    id_cli = request.POST['id_cli']
    nombres_cli = request.POST['nombres_cli']
    apeliidos_cli = request.POST['apeliidos_cli']
    telefono_cli = request.POST['telefono_cli']
    direccion_cli = request.POST['direccion_cli']
    correo_electronico_cli = request.POST['correo_electronico_cli']

    clienteUpdate = Cliente.objects.get(id_cli=id_cli)
    clienteUpdate.nombres_cli = nombres_cli
    clienteUpdate.apeliidos_cli = apeliidos_cli
    clienteUpdate.direccion_cli = direccion_cli
    clienteUpdate.telefono_cli = telefono_cli
    clienteUpdate.correo_electronico_cli = correo_electronico_cli
    clienteUpdate.save()

    messages.success(request, 'Cliente actualizado correctamente')
    return redirect('/cliente')

#TODO: Enviar correo
def enviarMensajeView(request):
    return render(request, 'enviarMensaje.html')

def enviar_correo(request):
    if request.method == 'POST':
        destinatario = request.POST['destinatario']
        asunto = request.POST['asunto']
        cuerpo = request.POST['cuerpo']

        send_mail(asunto, cuerpo, settings.EMAIL_HOST_USER, [destinatario], fail_silently=False)
        messages.success(request, 'Correo enviado correctamente')
        return redirect('/enviarMensaje')
    return redirect('/enviarMensaje')


#Todo: Platillo
def listadoPlatillo(request):
    platilloDbb = Platillo.objects.all()
    tiposPlatilloDbb = TiposPlatillo.objects.all()
    return render(request, 'platillo/listadoPlatillo.html', {
        'platillo': list(enumerate(platilloDbb, start=1)),
        'tiposplatillos': tiposPlatilloDbb
    })

def crearPlatillo(request):
    id_tipla = request.POST['id_tipla']
    tipoplatilloSeleccionado = TiposPlatillo.objects.get(id_tipla=id_tipla)
    nombre_pla = request.POST['nombre_pla']
    descripcion_pla = request.POST['descripcion_pla']
    precio_pla = request.POST['precio_pla']
    imagen_pla = request.FILES.get('imagen_pla')

    Platillo.objects.create(nombre_pla=nombre_pla, descripcion_pla=descripcion_pla, precio_pla=precio_pla, tiposplatillos=tipoplatilloSeleccionado, imagen_pla=imagen_pla)
    messages.success(request, 'Platillo agregado correctamente')
    return redirect('/platillo')

def eliminarPlatillo(request, id_pla):
    platilloDb = Platillo.objects.get(id_pla=id_pla)
    platilloDb.delete()
    messages.success(request, 'Platillo eliminado correctamente')
    return redirect('/platillo')

def actualizarPlatilloView(request, id_pla):
    platilloDb = Platillo.objects.get(id_pla=id_pla)
    tipoPlatilloDbb = TiposPlatillo.objects.all()

    return render(request, 'platillo/editarPlatillo.html', {
        'platillo': platilloDb, 'tiposplatillos': tipoPlatilloDbb
    })

def procesarActualizacionPlatillo(request):
    id_pla = request.POST['id_pla']
    id_tipla = request.POST['id_tipla']
    tipoplatilloSeleccionado = TiposPlatillo.objects.get(id_tipla=id_tipla)
    nombre_pla = request.POST['nombre_pla']
    descripcion_pla = request.POST['descripcion_pla']
    precio_pla = request.POST['precio_pla']
    imagen_pla = request.FILES.get('imagen_pla')

    platilloUpdate = Platillo.objects.get(id_pla=id_pla)
    platilloUpdate.tiposplatillos = tipoplatilloSeleccionado
    platilloUpdate.nombre_pla = nombre_pla
    platilloUpdate.descripcion_pla = descripcion_pla
    platilloUpdate.precio_pla = precio_pla
    if imagen_pla is not None:
        platilloUpdate.imagen_pla = imagen_pla

    platilloUpdate.save()

    messages.success(request, 'Platillo actualizado correctamente')
    return redirect('/platillo')


#Todo: Reserva
def listadoReserva(request):
    reservaDbb = Reserva.objects.all()
    clienteDbb = Cliente.objects.all()
    mesaDbb = Mesa.objects.all()
    return render(request, 'reserva/listadoReserva.html', {
        'reserva': list(enumerate(reservaDbb, start=1)),
        'cliente': clienteDbb,
        'mesa': mesaDbb
    })

def crearReserva(request):
    id_cli = request.POST['id_cli']
    clienteSeleccionado = Cliente.objects.get(id_cli=id_cli)
    id_mes = request.POST['id_mes']
    mesaSeleccionado = Mesa.objects.get(id_mes=id_mes)
    fecha_res = request.POST['fecha_res']
    numero_personas_res = request.POST['numero_personas_res']
    estado_res = request.POST['estado_res']

    Reserva.objects.create(fecha_res=fecha_res, numero_personas_res=numero_personas_res, estado_res=estado_res, cliente=clienteSeleccionado, mesa=mesaSeleccionado)
    messages.success(request, 'Reserva agregada correctamente')
    return redirect('/reserva')

def eliminarReserva(request, id_res):
    reservaDb = Reserva.objects.get(id_res=id_res)
    reservaDb.delete()
    messages.success(request, 'Reserva eliminada correctamente')
    return redirect('/reserva')

def actualizarReservaView(request, id_res):
    reservaDb = Reserva.objects.get(id_res=id_res)
    clienteDbb = Cliente.objects.all()
    mesaDbb = Mesa.objects.all()

    return render(request, 'reserva/editarReserva.html', {
        'reserva': reservaDb, 'cliente': clienteDbb, 'mesa': mesaDbb
    })

def procesarActualizacionReserva(request):
    id_res = request.POST['id_res']
    id_cli = request.POST['id_cli']
    clienteSeleccionado = Cliente.objects.get(id_cli=id_cli)
    id_mes = request.POST['id_mes']
    mesaSeleccionado = Mesa.objects.get(id_mes=id_mes)
    fecha_res = request.POST['fecha_res']
    numero_personas_res = request.POST['numero_personas_res']
    estado_res = request.POST['estado_res']

    reservaUpdate = Reserva.objects.get(id_res=id_res)
    reservaUpdate.cliente = clienteSeleccionado
    reservaUpdate.mesa = mesaSeleccionado
    reservaUpdate.fecha_res = fecha_res
    reservaUpdate.numero_personas_res = numero_personas_res
    reservaUpdate.estado_res = estado_res

    reservaUpdate.save()

    messages.success(request, 'Reserva actualizada correctamente')
    return redirect('/reserva')


#Todo: Pedido
def listadoPedido(request):
    pedidoDbb = Pedido.objects.all()
    platilloDbb = Platillo.objects.all()
    clienteDbb = Cliente.objects.all()
    return render(request, 'pedido/listadoPedido.html', {
        'pedido': list(enumerate(pedidoDbb, start=1)),
        'platillo': platilloDbb,
        'cliente': clienteDbb
        
    })

def crearPedido(request):
    id_pla = request.POST['id_pla']
    platilloSeleccionado = Platillo.objects.get(id_pla=id_pla)
    id_cli = request.POST['id_cli']
    clienteSeleccionado = Cliente.objects.get(id_cli=id_cli)
    fecha_ped = request.POST['fecha_ped']
    estado_ped = request.POST['estado_ped']
    comentarios_ped = request.POST['comentarios_ped']

    Pedido.objects.create(fecha_ped=fecha_ped, estado_ped=estado_ped, comentarios_ped=comentarios_ped, platillo=platilloSeleccionado, cliente=clienteSeleccionado)
    messages.success(request, 'Pedido agregado correctamente')
    return redirect('/pedido')

def eliminarPedido(request, id_ped):
    pedidoDb = Pedido.objects.get(id_ped=id_ped)
    pedidoDb.delete()
    messages.success(request, 'Pedido eliminado correctamente')
    return redirect('/pedido')

def actualizarPedidoView(request, id_ped):
    pedidoDb = Pedido.objects.get(id_ped=id_ped)
    platilloDbb = Platillo.objects.all()
    clienteDbb = Cliente.objects.all()

    return render(request, 'pedido/editarPedido.html', {
        'pedido': pedidoDb, 'platillo': platilloDbb, 'cliente': clienteDbb
    })

def procesarActualizacionPedido(request):
    id_ped = request.POST['id_ped']
    id_cli = request.POST['id_cli']
    clienteSeleccionado = Cliente.objects.get(id_cli=id_cli)
    id_pla = request.POST['id_pla']
    platilloSeleccionado = Platillo.objects.get(id_pla=id_pla)
    fecha_ped = request.POST['fecha_ped']
    estado_ped = request.POST['estado_ped']
    comentarios_ped = request.POST['comentarios_ped']

    pedidoUpdate = Pedido.objects.get(id_ped=id_ped)
    pedidoUpdate.cliente = clienteSeleccionado
    pedidoUpdate.platillo = platilloSeleccionado
    pedidoUpdate.fecha_ped = fecha_ped
    pedidoUpdate.estado_ped = estado_ped
    pedidoUpdate.comentarios_ped = comentarios_ped

    pedidoUpdate.save()

    messages.success(request, 'Pedido actualizado correctamente')
    return redirect('/pedido')


#Todo: Factura
def listadoFactura(request):
    facturaDbb = Factura.objects.all()
    pedidoDbb = Pedido.objects.all()
    return render(request, 'factura/listadoFactura.html', {
        'factura': list(enumerate(facturaDbb, start=1)),
        'pedido': pedidoDbb
        
    })

def crearFactura(request):
    id_ped = request.POST['id_ped']
    pedidoSeleccionado = Pedido.objects.get(id_ped=id_ped)
    fecha_fac = request.POST['fecha_fac']
    metodo_pago_fac = request.POST['metodo_pago_fac']

    Factura.objects.create(fecha_fac=fecha_fac, metodo_pago_fac=metodo_pago_fac, pedido=pedidoSeleccionado)
    messages.success(request, 'Factura agregada correctamente')
    return redirect('/factura')

def eliminarFactura(request, id_fac):
    facturaDb = Factura.objects.get(id_fac=id_fac)
    facturaDb.delete()
    messages.success(request, 'Factura eliminada correctamente')
    return redirect('/factura')

def actualizarFacturaView(request, id_fac):
    facturaDb = Factura.objects.get(id_fac=id_fac)
    pedidoDbb = Pedido.objects.all()

    return render(request, 'factura/editarFactura.html', {
        'factura': facturaDb, 'pedido': pedidoDbb
    })

def procesarActualizacionFactura(request):
    id_fac = request.POST['id_fac']
    id_ped = request.POST['id_ped']
    pedidoSeleccionado = Pedido.objects.get(id_ped=id_ped)
    fecha_fac = request.POST['fecha_fac']
    metodo_pago_fac = request.POST['metodo_pago_fac']

    facturaUpdate = Factura.objects.get(id_fac=id_fac)
    facturaUpdate.pedido = pedidoSeleccionado
    facturaUpdate.fecha_fac = fecha_fac
    facturaUpdate.metodo_pago_fac = metodo_pago_fac

    facturaUpdate.save()

    messages.success(request, 'Factura actualizada correctamente')
    return redirect('/factura')